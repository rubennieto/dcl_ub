# README #

Trabajo final de Grado. Universidad de Barcelona. RUBÉN NIETO. 

### Resumen ###

Este código corresponde con mi proyecto final de grado para la Universidad de Barcelona. 

En este proyecto, dentro del marco de la Marató de TV3, se pretende desarrollar una herramienta basada en memorias digitales para el entrenamiento cognitivo de la gente mayor afectada por deterioro cognitivo leve. Por desgracia,  estas personas están muy expuestas al riesgo de padecer Alzheimer. El objetivo del entrenamiento cognitivo basado en memoria digital es de retrasar el deterioro de la memoria y de las funciones cognitivas en general. Los efectos de esta enfermedad son devastadores, por ello, es muy necesario la interacción con otras personas.

La imágenes de interacciones sociales, debido a la carga emocional que conllevan, desencadenan una fuerte respuesta de la memoria autobiográfica y por lo tanto son muy adecuadas para estimularla. 

Por lo tanto, en este trabajo desarrollamos un algoritmo que, dado un conjunto de imágenes capturadas por un paciente a través de una cámara portátil durante un largo periodo de tiempo (mínimo dos semanas), se determina con cuántas personas suele interactuar y con quienes interactúa más. Para cada persona se selecciona un conjunto de imágenes que se utilizarán para que el paciente sea capaz de reconocer sus familiares en diferentes contextos y pueda revivir el momento y consolidar su memoria.

Para cada persona con la cual el paciente suele interactuar, se selecciona un conjunto de imágenes que se utilizarán para que el paciente sea capaz de reconocerla en diferentes contextos, pueda revivir el momento y consolidar su memoria.

Este trabajo me ha servido para profundizar en dos aspectos que son vitales para mi visión del mundo. En primer lugar, poder colaborar i ayudar a la gente mayor, que son los responsables directos de ser quienes somos, el otro aspecto, es la visión por computador i el aprendizaje automático.

### Contribuciones ###

Se han utilizado diferentes Frameworks para el desarrollo de este proyecto

* **OpenFace**: se ha utilizado para la detección facial y la clasificación
* **Face detection pose estimation and landmark localization in the wild**: se ha utilizado a modo de comparación con OpenFace
* También se han utilizado diversas funciones de la librería **scikit-learn**
* Y se han visto los algoritmos para la reducción de dimensionalidad **PCA** y **t-SNE**


### Manual de usuario ###

Se trata de una aplicación desarrollada en Python la cual cuenta con un menú por consola y con él podremos obtener las características de las imágenes, calcular las distancias entre las caras obtenidas, entre otros.

![menu1.png](https://bitbucket.org/repo/dGkaxX/images/3609125651-menu1.png)

Como podemos ver en la imagen anterior, disponemos de un menú con ocho opciones. Estos pasos, por lo general, son secuenciales, es decir, necesitaremos tener el paso anterior para poder realizar el siguiente, esto está diseñado así debido a que, si queremos hacer pruebas podemos ejecutar diversos pasos cerrar la aplicación, para más tarde, si lo deseamos, continuar por donde nos habíamos quedado.

En la primera opción, podremos calcular las características de las imágenes. Es decir, introduciendo la ruta donde se encuentran las imágenes en el fichero de configuración \textit{tfg.ini}, podremos obtener la detección facial y extraer las características de dichas imágenes. 

En segundo lugar, podremos calcular las distancias entre las caras encontradas de nuestro dataset. 

En tercer lugar, podremos hacer una reducción de la dimensionalidad con las características que hemos obtenido al ejecutar el paso 1. Este paso, nos llevará a un submenú para que podamos indicar el método que queramos utilizar. Los cuales pueden ser, PCA o t-SNE.

![mds.png](https://bitbucket.org/repo/dGkaxX/images/1556931915-mds.png)

En el cuarto elemento de nuestro menú encontramos con dos opciones, la primera crear los clusters de forma no supervisada con diferentes métodos o por el contrario, crear una clasificación con aprendizaje supervisado.

En el siguiente punto del menú nos encontramos el cálculo del *accuracy* y *recall*. El cual, también obtendremos los True Positive, True Negative, False Positive y False Negative.

En el sexto punto del menú tenemos la interfície gráfica con la ayuda de **Tkinter** para visualizar las diferentes personas que tenemos en nuestro dataset.

Y por útlimo, tenemos la opción de obtener la evaluación de los métodos utilizados, entre ellos se encuentra el clustering y la clasificación.

![marcav_pos_rgb.jpg](https://bitbucket.org/repo/dGkaxX/images/3967626929-marcav_pos_rgb.jpg)