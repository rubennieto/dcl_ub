# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import constants
import datetime
import logging as log
import os

def logs():
    """
    Método para inicializar los logs
    :return:
    """
    LOG_FILENAME = constants.LOGS + constants.FILELOG

    if os.path.isfile(LOG_FILENAME):
        now = datetime.datetime.now()
        now = now.strftime("%Y-%m-%d_%H:%M")
        os.rename(LOG_FILENAME, LOG_FILENAME + '.' + str(now))
    global logger
    logger = log.getLogger(__name__)
    hdlr = log.FileHandler(LOG_FILENAME)
    formatter = log.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(constants.LEVEL_LOG)
    logger.propagate = False

def get_logger():
    """
    Retornamos la variable logger
    :return:
    """
    global logger
    return logger