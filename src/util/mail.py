# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import smtplib
import urllib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename

from email.MIMEImage import MIMEImage

from src.util import constants


def message(folder, file):
    return urllib.urlopen(folder + file).read()

def py_mail(SUBJECT, BODY, TO, FROM, pw, files = None):
    """Con esta función enviamos enviamos el mail para informar"""

    # Create message container - the correct MIME type is multipart/alternative here!
    MESSAGE = MIMEMultipart('alternative')
    MESSAGE['subject'] = SUBJECT
    MESSAGE['To'] = TO
    MESSAGE['From'] = FROM
    MESSAGE.preamble = """Su buzón de correo no soporta este correo. Póngase en contacto con tfg.alzheimer@gmail.com"""

    for f in files or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
            MESSAGE.attach(part)

    HTML_BODY = MIMEText(BODY, 'html')
    fp = open('../resources/mail/images/logo.png', 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()

    msgImage.add_header('Content-ID', '<image1>')
    MESSAGE.attach(msgImage)
    MESSAGE.attach(HTML_BODY)

    server = smtplib.SMTP('smtp.gmail.com:587')

    server.starttls()
    server.login(FROM, pw)
    server.sendmail(FROM, [TO], MESSAGE.as_string())
    server.quit()


def send(correct=True, files = None):
    """
    Esta función necesitara por parámetros si es correcto, es decir, si la ejecución se ha ejecturado correctamente o,
    por lo contrario si ha habido algun error, poder avisar.
    :param correct:
    :param files:
    :return:
    """

    if not correct:
        py_mail("Error en la ejecución", message(constants.MAIL, constants.KO), constants.TO, constants.FROM, constants.PW, files)
    else:
        py_mail("Resultados ejecución", message(constants.MAIL, constants.OK), constants.TO, constants.FROM, constants.PW, files)




