# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

"""
Este fichero sirve para poder comunicarnos con el fichero de configuración tfg.ini
"""
import ConfigParser

config = ConfigParser.ConfigParser()
config.readfp(open('../tfg.ini'))

# Folders
INPUT = config.get('Folders', 'input', 0)
OUTPUT = config.get('Folders', 'output', 0)
BOUNDINGBOX = config.get('Folders', 'boundingBox', 0)
CLUSTERS = config.get('Folders', 'clusters', 0)
FOLDERFEATURES = config.get('Folders', 'features', 0)
DETECTION = config.get('Folders', 'detection', 0)
PROTOTYPES = config.get('Folders', 'prototypes', 0)
MAT = config.get('Folders', 'mat', 0)
MAIL = config.get('Folders', 'mail', 0)
IMAGES = config.get('Folders', 'images', 0)
REP = config.get('Folders', 'rep', 0)
MAT_MATLAB = config.get('Folders', 'mat_matlab', 0)
DATASETS = config.get('Folders', 'datasets', 0)
DIST = config.get('Folders', 'dist', 0)
PERSONS = config.get('Folders', 'persones', 0)
BOT = config.get('Folders', 'bot', 0)

# Files
FILEFEATURES = config.get('Files', 'fileFeatures', 0)
IMAGECLUSTERING = config.get('Files', 'clustering', 0)
OK = config.get('Files', 'mailOK', 0)
KO = config.get('Files', 'mailKO', 0)
EXCEL = config.get('Files', 'excel', 0)
DATASET = config.get('Files', 'dataset', 0)

# Mailing
TO = config.get('Mail', 'to', 0)
FROM = config.get('Mail', 'from', 0)
PW = config.get('Mail', 'pw', 0)

# Logs
LOGS = config.get('Folders', 'logs', 0)
FILELOG = config.get('Files', 'fileLog', 0)
FILETEST = config.get('Files', 'fileTest', 0)
LEVEL_LOG = config.get('LOG', 'level', 0)

# INFO
ABOUT = config.get('Info', 'about', 0)