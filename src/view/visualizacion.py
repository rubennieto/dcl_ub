#!/usr/bin/env python
# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import calendar
import datetime
import time
import tkFont
import tkMessageBox
import ttk
from PIL import ImageTk, Image
import PIL
from Tkinter import *
from os import listdir
from os.path import exists
from ttk import *
from os.path import isfile

from src.util import constants
from src.util import exiftool


class GUI:
    """
    En esta clase crearemos una interfície gráfica para visualizar las personas que tenemos
    en nuestros dataset. Para ello, utilizaremos la librería Tkinter
    """
    def __init__(self):
        """
        Constructor de la clase.
        Inicializamos los componentes que forman la interfície gráfica.
        """
        self.__path = '../images/marcav_pos_rgb.jpg'
        self.__dicc = dict()
        self.__count = 0

        color = '#F2F2F2'

        self.__window = Tk()
        self.__window.title("Imagenes")
        self.__window.geometry("750x550")
        self.__window.configure(background=color)

        self.__menu()

        s = Style()
        s.configure('My.TFrame', background= color)

        self.__filters = Frame(self.__window, style='My.TFrame')
        self.__filters.pack(side = TOP)

        self.__content = Frame(self.__window, style='My.TFrame')
        self.__content.pack(side = TOP)

        self.__footer = Frame(self.__window, style='My.TFrame')
        self.__footer.pack(side = TOP)

        #### COMBOBOX
        subfolder = [folder for folder in listdir(constants.PERSONS) if exists(constants.PERSONS + folder) and not isfile(constants.PERSONS + folder)]

        variable = StringVar(self.__filters)
        variable.set("")

        self.__cbx = Combobox(self.__filters, textvariable=variable, values=subfolder)
        self.__cbx.pack(side=LEFT)

        ### BUTTON SEARCH
        photoSearch = PhotoImage(file='../images/boton-de-reproduccion.png')
        self.__botonFilter = Button(self.__filters, image = photoSearch, command= lambda: self.__search(self.__cbx.get()))
        self.__botonFilter.pack(side = RIGHT)

        ### LEFT IMAGE
        self.__panelLeft = Label(self.__content)
        self.__panelLeft.pack(side = LEFT)

        ### BUTTON LEFT
        photoLeft = PhotoImage(file='../images/espalda.png')
        botonLeft = Button(self.__content, image=photoLeft, command=self.__left)
        botonLeft.pack(side = LEFT)

        ### CENTRAL IMAGE
        image = PIL.Image.open(open(self.__path, 'rb'))
        image = image.resize((270, 350), PIL.Image.ANTIALIAS)
        img = ImageTk.PhotoImage(image)

        self.__panel = Label(self.__content, image = img)
        self.__panel.pack(side = LEFT)

        ### BUTTON RIGHT
        photoRight = PhotoImage(file='../images/proximo.png')
        self.__botonRight = Button(self.__content, image = photoRight, command=self.__right)
        self.__botonRight.pack(side = LEFT)

        ### RIGHT IMAGE
        self.__panelRight = Label(self.__content)
        self.__panelRight.pack(side = LEFT)

        ### DATE
        self.__date = Label(self.__footer, text = '')
        self.__date.pack(side = TOP)

        curtime = time.localtime()

        yearInt = curtime[0]
        monthInt = curtime[1]
        HLayout = ttk.PanedWindow(self.__footer, orient=HORIZONTAL)
        self.__tx = Text(self.__footer, padx=10, pady=10, bg="#f3e9ae", relief=FLAT, height=9,
                           width=20)  # text view to passing to functions

        self.init_calendar(yearInt, monthInt)

        self.__tx.pack()
        HLayout.pack()

        self.__window.mainloop()

    def __menu(self):
        """
        En este método inicializamos el menú que tendremos en nuestra aplicación
        :return:
        """
        # Crear el menu principal
        menubarra = Menu(self.__window)

        # Crea un menu desplegable y lo agrega a la barra superior
        menuayuda = Menu(menubarra, tearoff=0)
        menuayuda.add_command(label="Acerca de...", command=self.__info)
        menubarra.add_cascade(label="Ayuda", menu=menuayuda)

        # Mostrar el menu
        self.__window.config(menu=menubarra)

    def __info(self):
        """
        Método que muestra la información
        :return:
        """
        ABOUT_TEXT = constants.ABOUT

        ### POP UP
        tkMessageBox.showinfo(
            "Información",
            ABOUT_TEXT
        )
        return

    def __get_date(self):
        """
         Este método retorna fecha de creación de la imagen
        """
        fecha = self.Meta()[:-6]
        fechas = fecha.split(' ')
        dia = fechas[0].replace(':', '-')
        str = dia + ' ' + fechas[1]

        datetime_object = datetime.datetime.strptime(str, '%Y-%m-%d %H:%M:%S')
        return datetime_object

    def Meta(self):
        """
        Método para obtener mediante los metadatos la fecha de creación
        :return:
        """
        files = []
        files.append(self.__path)
        with exiftool.ExifTool() as et:
            metadata = et.get_metadata_batch(files)
        for d in metadata:
            return ("{}".format(d["File:FileModifyDate"]))

    def __show_image(self, center, left, right):
        """
        Mostramos la imágenes
        :param center:
        :param left:
        :param right:
        :return:
        """
        # CENTRAL IMAGE
        image = PIL.Image.open(self.__dicc[center])
        image = image.resize((270, 350), PIL.Image.ANTIALIAS)
        img = ImageTk.PhotoImage(image)

        self.__panel.configure(image=img)
        self.__panel.image = img

        # LEFT IMAGE
        imageLeft = PIL.Image.open(self.__dicc[left])
        imageLeft = imageLeft.resize((192, 250), PIL.Image.ANTIALIAS)
        imgLeft = ImageTk.PhotoImage(imageLeft)

        self.__panelLeft.configure(image=imgLeft)
        self.__panelLeft.image = imgLeft

        # RIGTH IMAGE
        imageRight = PIL.Image.open(self.__dicc[right])
        imageRight = imageRight.resize((192, 250), PIL.Image.ANTIALIAS)
        imgRight = ImageTk.PhotoImage(imageRight)

        self.__panelRight.configure(image=imgRight)
        self.__panelRight.image = imgRight

        str = self.__get_date()
        self.__date.configure(text=str)

    def __search(self, name):
        """
        Método que busca las imagenes de la carpeta seleccionada
        :param name:
        :return:
        """
        self.__dicc.clear()
        images = ['../resources/classifier/persons/' + name + '/' + i for i in listdir('../resources/classifier/persons/' + name + '/') if re.match(r'[0-9]+.*\.jpg', i)]

        fechas = []
        days = []
        for i in range(len(images)):
            self.__path = images[i]
            # date = self.__get_date()

            date = images[i].split('/')
            date = date[-1].split('_')[1]
            date = date[:4] + '-' + date[4:6] + '-' + date[6:8]
            datetime_object = datetime.datetime.strptime(date, '%Y-%m-%d')
            fechas.append((self.__path, datetime_object))
            y = datetime_object.year
            m = datetime_object.month
            days.append(datetime_object.day)
        fechas_sort = sorted(fechas, key=lambda t: t[1])

        for i in range(len(fechas_sort)):
            self.__dicc[i] = fechas_sort[i][0]

        try:
            self.__show_image(0, len(self.__dicc) - 1, 1)
            self.update(y, m, days)
        except:
            print "No hay imagenes para mostrar"



    def __right(self):
        """
        Método para mover las imágenes a la derecha
        :return:
        """
        tmp = self.__count
        try:
            left = self.__count
            self.__count += 1

            if self.__count >= len(self.__dicc):
                self.__count = 0

            right = self.__count + 1
            if right >= len(self.__dicc):
                right = 0

            if left == -1: left = len(self.__dicc) - 1
            self.__show_image(self.__count, left, right)
        except:
            self.__count = tmp
            tkMessageBox.showwarning(
                "Error al intentar pasar la foto...",
                "Prueba cargando un nuevo directorio y dale al play"
            )
            return

    def __left(self):
        """
        Método para mover las imágenes a la izquierda
        :return:
        """
        tmp = self.__count
        try:
            right = self.__count
            self.__count -= 1

            if self.__count < 0:
                self.__count = len(self.__dicc) - 1

            left = self.__count - 1
            if left < 0:
                left = len(self.__dicc) - 1
            self.__show_image(self.__count, left, right)
        except:
            self.__count = tmp
            tkMessageBox.showwarning(
                "Error al intentar pasar la foto...",
                "Prueba cargando un nuevo directorio y dale al play"
            )
            return

    def init_calendar(self, y, m):
        """
        Inicializamos el calendario
        :param y:
        :param m:
        :return:
        """
        calstr = calendar.month(y, m)
        self.__tx.configure(state=NORMAL)
        self.__tx.delete('0.0', END)  # remove previous calendar
        self.__tx.insert(INSERT, calstr)
        for i in range(2, 9):
            self.__tx.tag_add("others", '{}.0'.format(i), '{}.end'.format(i))  # tag days for coloring
            if len(self.__tx.get('{}.0'.format(i), '{}.end'.format(i))) == 20:
                self.__tx.tag_add("sun", '{}.end-2c'.format(i), '{}.end'.format(i))
        self.__tx.tag_config("sun", foreground="#fb4622")
        self.__tx.tag_config("others", foreground="#427eb5")
        self.__tx.tag_add("head", '1.0', '1.end')
        segoe = tkFont.Font(family='Segoe UI')
        self.__tx.tag_config("head", font=segoe, foreground="#0d8241", justify=CENTER)
        self.__tx.configure(state=DISABLED)  # make text view not editable

    def update(self, y, m, list):
        """
        Actualizamos el calendario con las fechas que se ha interactuado
        :param y:
        :param m:
        :param list:
        :return:
        """
        try:
            calstr = calendar.month(y, m)
            self.__tx.configure(state=NORMAL)
            self.__tx.delete('0.0', END)  # remove previous calendar
            self.__tx.insert(INSERT, calstr)
            for i in range(2, 9):
                self.__tx.tag_add("others", '{}.0'.format(i), '{}.end'.format(i))  # tag days for coloring
                if len(self.__tx.get('{}.0'.format(i), '{}.end'.format(i))) == 20:
                    self.__tx.tag_add("sun", '{}.end-2c'.format(i), '{}.end'.format(i))
            self.__tx.tag_config("sun", foreground="#fb4622")
            self.__tx.tag_config("others", foreground="#427eb5")
            self.__tx.tag_add("head", '1.0', '1.end')

            for i in list:
                index = self.__tx.search(str(i), '2.0')  # search for today's date
                self.__tx.tag_add("cur", index, '{}+2c'.format(index))  # highlight today's date
                self.__tx.tag_config("cur", background="green", foreground="white")
            segoe = tkFont.Font(family='Segoe UI')
            self.__tx.tag_config("head", font=segoe, foreground="#0d8241", justify=CENTER)
            self.__tx.configure(state=DISABLED)  # make text view not editable
        except:
            print "error en el calendario"
