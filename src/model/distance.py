# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import logging as log
import numpy as np
import os
from os import listdir
from os.path import exists
from os.path import isfile

import xlsxwriter

from src.util import constants
from src.util import logs

def distance():
    """
    Esta función calcula la matriz de distancia a partir de las características de cada cara.
    :return:
    """
    logger = logs.get_logger()
    logger.info('Obteniendo matriz de distancias...')

    if exists(constants.BOUNDINGBOX):
        # Obtenemos las caras
        imgs = [im for im in listdir(constants.BOUNDINGBOX) if isfile(constants.BOUNDINGBOX + im)]
        features = [[0 for m in range(len(imgs) + 1)] for x in range(len(imgs))]
        filename = constants.FOLDERFEATURES + constants.FILEFEATURES

        # Para cada par de imagenes calculamos su distancia
        for i in range(len(imgs)):
            features[i][0] = imgs[i][:-4]
            x = np.loadtxt(constants.FOLDERFEATURES + imgs[i][:-4] + '.txt', delimiter=',')
            # for j in range(i): # Matriz triangular superior
            for j in range(1, len(imgs) + 1):  # Matriz simetricas
                y = np.loadtxt(constants.FOLDERFEATURES + imgs[j - 1][:-4] + '.txt', delimiter=',')
                d = x - y
                features[i][j] = np.dot(d,d)
                logger.debug("Comparing {} with {}.".format(imgs[i], imgs[j-1]))
                logger.debug("  + Squared l2 distance between representations: {:0.3f}".format(np.dot(d, d)))

        # En caso de no tener imagenes informamos mediante los logs, sino guardamos el fichero.
        if len(imgs) != 0:
            np.savetxt(filename, np.asarray(features), fmt='%s', delimiter=';')
            logger.info('Creat fitxer de caracteristiques a la carpeta features')
        else:
            logger.info('No hi ha imatges per calcular la distancia')
        __generaExcel()
    else:
        print "No such folder of bounding boxes, try execute the first item of menu."
        logger.info("No such folder of bounding boxes, try execute the first item of menu.")


def __generaExcel():
    """
    En esta función generamos un Excel para tener de forma visual la matriz de características.
    :return:
    """
    try:
        # Creamos el excel y le damos formato
        workbook = xlsxwriter.Workbook(constants.FOLDERFEATURES + constants.EXCEL)
        worksheet = workbook.add_worksheet()
        format = workbook.add_format()
        format.set_align('center')

        # Las imagenes irán en la primera columna y en la primera fila
        imgs = [constants.BOUNDINGBOX + im for im in os.listdir(constants.BOUNDINGBOX) if os.path.isfile(
            constants.BOUNDINGBOX + im)]

        row = 1
        col = 0
        for item in (imgs):
            worksheet.write(row, 0, item, format)
            worksheet.write(0, col, item, format)
            col += 1
            row += 1

        # Le damos un espacio para su facil lectura
        worksheet.set_column(0, col, 41)
        # Leemos el fichero de caracteristicas
        file = open(constants.FOLDERFEATURES + constants.FILEFEATURES, 'r')

        row = 1
        col = 1
        lists = file.read().split('\n')
        for i in lists[:-1]:
            for j in i.split(','):
                worksheet.write(row, col, j, format)
                col += 1
            col = 1
            row +=1

        file.close()
        workbook.close()
    except:
        print "No ha sido posible crear el Excel"
        log.info("No ha sido posible crear el Excel.")