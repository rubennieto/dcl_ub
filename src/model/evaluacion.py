# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################


import logging as log
import openpyxl
from sklearn.metrics.cluster import normalized_mutual_info_score

from src.util import constants
from src.util import logs
from os import listdir
from os.path import isfile
import os

def eval(option = True):
    """
    Este método evalua los clusters o la clasificación depende de la opcion elegida. Leemos el excel creado y lo comparamos
    con los resultados de los clusters executados
    :param option:
    :return:
    """

    logs.logs()
    logger = logs.get_logger()
    fileDir = os.path.dirname(os.path.realpath(__file__))
    doc = openpyxl.load_workbook(os.path.join(fileDir,'../../resources/GT4cluster.xlsx'))
    try:
        hoja = doc.get_sheet_by_name('Sheet1')
        dicc1 = dict()
        # Recorremos el excel
        for i in range(2, 2034):
            n = hoja.cell(row=i, column=2).value
            name = '0_' + str(n)
            value = hoja.cell(row=i, column=7).value
            if name in dicc1:
                numero = int(name[:1]) + 1
                name = str(numero) + name[1:]
            dicc1[name] = str(int(value))

        if option:
            # Evaluamos los clusters
            for folder in listdir(constants.CLUSTERS):
                if not isfile(constants.CLUSTERS + folder):
                    file = open(constants.CLUSTERS + folder + '/relation.txt', 'r')

                    dicc2 = dict()
                    for line in file:
                        linea = line.split('\t')
                        value = linea[1][:2]
                        nam = linea[0]

                        # Descomentar las siguientes lineas para evaluar BoT
                        # nam = '0_' + str(linea[0])
                        # if nam in dicc2:
                        #     numero = int(nam[:1]) + 1
                        #     nam = str(numero) + nam[1:]
                        if '\n' in value:
                            value = value[:-1]
                        dicc2[nam] = value

                    listaGT = []
                    listaPr = []

                    for key in dicc2:
                        if key in dicc1:
                            listaGT.append(dicc1[key])
                            listaPr.append(dicc2[key])

                    print "The result of evaluation of the " + folder + " is {0:.4f}".format(
                        normalized_mutual_info_score(listaGT, listaPr))
                    logger.info("The result of evaluation " + folder + " is {0:.4f}".format(normalized_mutual_info_score(listaGT, listaPr)))
        else:
            # Evaluamos la clasificación
            file = open(constants.PERSONS + 'relation.txt', 'r')

            dicc2 = dict()
            for line in file:
                linea = line.split('\t')
                value = linea[1][:2]
                if '\n' in value:
                    value = value[:-1]
                dicc2[linea[0]] = value

            listaGT = []
            listaPr = []

            for key in dicc2:
                if key in dicc1:
                    listaGT.append(dicc1[key])
                    listaPr.append(dicc2[key])

            print "The result of evaluation of the is {0:.4f}".format(normalized_mutual_info_score(listaGT, listaPr))
            logger.info("The result of evaluation is {0:.4f}".format(normalized_mutual_info_score(listaGT, listaPr)))


    except IOError:
        print "No such files for evaluation, try execute the clustering item"
        logger.info("No such files for evaluation, try execute the clustering item")
