# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import time

import numpy as np
import matplotlib.pyplot as plt

from sklearn import cluster, datasets
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler

from src.util import constants
from os import listdir, makedirs
from os.path import isfile, exists
import shutil
import os
import re
from src.util import tsne


def __mkdir(folder):
    """
    Método de soporte para comprobar la existencia de un directorio y sino crearlo
    :param folder:
    :return:
    """
    if not exists(folder):
        makedirs(folder)


def __mkdir_rm(folder):
    """
    Método de soporte para comprobar si un directorio existe y eliminarlo
    :param folder:
    :return:
    """
    if exists(folder):
        shutil.rmtree(folder)
    makedirs(folder)


def clusters(datasets):
    """
    Este método calcula diferentes métodos de clustering
    :param datasets:
    :return:
    """
    colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
    colors = np.hstack([colors] * 20)

    clustering_names = [
        'MiniBatchKMeans', 'AffinityPropagation', 'MeanShift',
        'SpectralClustering', 'Ward', 'AgglomerativeClustering',
        'DBSCAN', 'Birch']

    plt.figure(figsize=(len(clustering_names) * 2 + 3, 9.5))
    plt.subplots_adjust(left=.02, right=.98, bottom=.001, top=.96, wspace=.05,
                        hspace=.01)

    plot_num = 1

    imgs = [im for im in listdir(constants.BOUNDINGBOX) if isfile(constants.BOUNDINGBOX + im)]
    for i_dataset, dataset in enumerate(datasets):
        X, y = dataset
        X = np.asarray(X)
        print X
        # normalize dataset for easier parameter selection
        X = StandardScaler().fit_transform(X)

        # estimate bandwidth for mean shift
        bandwidth = cluster.estimate_bandwidth(X, quantile=0.3)

        # connectivity matrix for structured Ward
        connectivity = kneighbors_graph(X, n_neighbors=10, include_self=False)
        # make connectivity symmetric
        connectivity = 0.5 * (connectivity + connectivity.T)

        # create clustering estimators
        ms = cluster.MeanShift(bandwidth=bandwidth, bin_seeding=True)
        two_means = cluster.MiniBatchKMeans(n_clusters=2)
        ward = cluster.AgglomerativeClustering(n_clusters=2, linkage='ward',
                                               connectivity=connectivity)
        spectral = cluster.SpectralClustering(n_clusters=2,
                                              eigen_solver='arpack',
                                              affinity="nearest_neighbors")
        dbscan = cluster.DBSCAN(eps=.2)
        affinity_propagation = cluster.AffinityPropagation(damping=.9,
                                                           preference=-200)

        average_linkage = cluster.AgglomerativeClustering(
            linkage="average", affinity="cityblock", n_clusters=2,
            connectivity=connectivity)

        birch = cluster.Birch(n_clusters=2)
        clustering_algorithms = [
            two_means, affinity_propagation, ms, spectral, ward, average_linkage,
            dbscan, birch]

        for name, algorithm in zip(clustering_names, clustering_algorithms):
            # predict cluster memberships
            t0 = time.time()
            algorithm.fit(X)
            t1 = time.time()
            if hasattr(algorithm, 'labels_'):
                y_pred = algorithm.labels_.astype(np.int)
            else:
                y_pred = algorithm.predict(X)

            minim = min(y_pred)
            maxim = max(y_pred)
            for folder in range(minim, maxim + 1):
                makedirs(constants.CLUSTERS + name + '/' + str(folder))

            # my_dict = {i: np.asarray(y_pred).tolist().count(i) for i in y_pred}

            lista = []
            for i in range(len(y_pred)):
                shutil.copyfile(constants.BOUNDINGBOX + imgs[i],
                                constants.CLUSTERS + name + '/' + str(y_pred[i]) + '/' + imgs[i])
                lista.append((imgs[i], y_pred[i]))

            f = open(constants.CLUSTERS + name + '/relation.txt', 'w')
            for l in lista:
                f.write(str(l[0]) + '\t' + str(l[1]) + '\n')
            f.close()


            # plot
            plt.subplot(4, len(clustering_algorithms), plot_num)
            if i_dataset == 0:
                plt.title(name, size=18)
            plt.scatter(X[:, 0], X[:, 1], color=colors[y_pred].tolist(), s=10)

            if hasattr(algorithm, 'cluster_centers_'):
                centers = algorithm.cluster_centers_
                center_colors = colors[:len(centers)]
                plt.scatter(centers[:, 0], centers[:, 1], s=100, c=center_colors)
            plt.xlim(-2, 2)
            plt.ylim(-2, 2)
            plt.xticks(())
            plt.yticks(())
            plt.text(.99, .01, ('%.2fs' % (t1 - t0)).lstrip('0'),
                     transform=plt.gca().transAxes, size=15,
                     horizontalalignment='right')
            plot_num += 1

    plt.show()
    plt.savefig(constants.CLUSTERS + constants.IMAGECLUSTERING)


def read_features():
    """
    Con este método leemos las características que hemos obtenido y hacemos una reducción de dimensionalidad con t-SNE
    :return:
    """
    lista = []
    files = []
    if exists(constants.FOLDERFEATURES):
        for file in os.listdir(constants.FOLDERFEATURES):
            if re.match(r'[0-9]+.*\.txt', file):
                target = open(constants.FOLDERFEATURES + file, 'r+')
                str = target.read()
                lista.append([float(i) for i in str.split(',')])
                files.append(file[:-4])
                target.close()

    if len(lista) > 0:
        X = tsne.tsne(np.array(lista))
    else:
        X = []
    return X #, files


def cluster_by_file(faces):
    """
    Con este método eliminamos el directorio donde guardamos los clusters y llamamos para obtener los diferentes métodos
    :param lista:
    :return:
    """

    if len(faces) > 0:
        __mkdir_rm(constants.CLUSTERS)
        datasets = [(faces, None)]
        clusters(datasets)
    else:
        print "Lista para realizar el clustering vacía"