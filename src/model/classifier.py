# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import shutil
import subprocess
from os.path import exists
import os
from src.util import constants


def clas():
    """
    En esta función crearemos un clasificador siguiendo los pasos de OpenFace y llamando a los procesos para su creación
    :return:
    """
    fileDir = os.path.dirname(os.path.realpath(__file__))
    if exists(os.path.join(fileDir, constants.PERSONS + 'out_classifier.txt')):
        os.remove(os.path.join(fileDir, constants.PERSONS + 'out_classifier.txt'))

    out = os.path.join(fileDir, constants.PERSONS + 'out_classifier.txt')
    with open(out, "w+") as f:
        p1 = subprocess.check_call(['./../util/prune-dataset.py', '../resources/classifier/raw', '--numImagesThreshold', '3'], stdout=f)
        p2 = subprocess.check_call(["./../util/align-dlib.py", "../resources/classifier/raw/", "align", "outerEyesAndNose", "../resources/classifier/aligned", "--size", "96"], stdout=f)
        p3 = subprocess.check_call(['./../batch-represent/main.lua', '-outDir', '../resources/classifier/output', '-data', '../resources/classifier/aligned'], stdout=f)
        p4 = subprocess.check_call(['./../util/classifier.py', 'train', '../resources/classifier/output'], stdout=f)
