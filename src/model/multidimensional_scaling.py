# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import logging as log
import matplotlib.pyplot as plt
import numpy as np
import re
import shutil
import time
from os import listdir, makedirs
from os.path import isfile, exists

from src.util import constants
from src.util import tsne
from src.util import logs


def __mkdir(folder):
    """
    Función de soporte para eliminar carpeta en caso que exista y volverla a crear.
    :param folder:
    :return:
    """
    if exists(folder):
        shutil.rmtree(folder)
    makedirs(folder)


def redimension(option=1):
    """
    Esta función hace la lógica para poder hacer la reducción de dimensionalidad.
    Leemos las caracteristicas y llamamos, segun la opción elegida para hacerlo con t-SNE (opción 1) o con PCA (opción 2)
    :param option: opción para elegir el método de reducción de dimensionalidad
    :return:
    """
    logger = logs.get_logger()
    start = time.time()
    __mkdir(constants.DATASETS)

    list = []
    try:
        if exists(constants.FOLDERFEATURES):
            for folder in listdir(constants.FOLDERFEATURES + 'data/'):
               if not isfile(constants.FOLDERFEATURES + '/data/' + folder):
                   for file in listdir(constants.FOLDERFEATURES + 'data/' + folder):
                       if re.match(r'[0-9]+.*\.txt', file):
                           fitxer = constants.FOLDERFEATURES + 'data/' + folder + '/' + file
                           list.append(np.genfromtxt(fitxer, delimiter=','))

            list = np.array(list)
            # Guardamos todas las características juntas en un fichero
            np.savetxt(constants.FOLDERFEATURES + 'all_features.txt', list, delimiter=',')
            # Dependiendo de la opción llamamos a t-SNE o PCA
            if option == 1:
                Y = tsne.tsne(list, 2, 128)
            elif option == 2:
                Y = tsne.pca(list, 2)

            elapsed = time.time() - start
            logger.info('Tiempo total de la reducción: {} segundos'.format(round(elapsed, 3)))

            # Dibujamos el resultado
            plt.plot(Y[:,0], Y[:,1], 'ro')
            plt.show()

            if exists(constants.DATASETS + 'reduccion.png'):
                shutil.rmtree(constants.DATASETS + 'reduccion.png')
            plt.savefig(constants.DATASETS + 'reduccion.png')
            # Guardamos lo que nos retorna la reducción
            np.savetxt(constants.DATASETS + constants.DATASET, Y, delimiter=',')
        else:
            print "No such folder of features, try execute the first item of menu."
            logger.info("No such folder of features, try execute the first item of menu.")
    except:
        print "Ha ocurrido un error, es posible que no exista la ruta /resources/data/"
        log.info("Ha ocurrido un error, es posible que no exista la ruta /resources/data/")