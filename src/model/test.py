# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

#################################################
## IMPORTANTE: Parte del código para el cálculo del área de los rectángulos ha sido extraído de:
## http://www.pyimagesearch.com/2016/11/07/intersection-over-union-iou-for-object-detection/
#################################################

import logging as log
import numpy as np
import os
import re
import scipy.io as sio
from collections import namedtuple
from os.path import exists

import cv2

from src.util import constants
from src.util import logs


def __read_matlab():
    """
    Esta función lee los ficheros del Ground Truth guardados en las diferentes secuencias
    :return:
    """
    logger = logs.get_logger()
    dicc = {}
    # Obtenemos las imagenes
    folders = [constants.INPUT + folder for folder in os.listdir(constants.INPUT) if not os.path.isfile(folder)]
    subfolder = [folder + '/' + i for folder in folders for i in os.listdir(folder) if not os.path.isfile(folder)]
    images = [folder + '/' + i for folder in subfolder for i in os.listdir(folder) if
              os.path.isfile(folder + '/' + i) and re.match(r'[0-9]+.*\.jpg', i)]

    # Obtenemos el numero de secuencias
    # No nos interesa los nombres de las carpetas sino el numero de carpetas que tenemos
    folders = [folder for folder in os.listdir(constants.INPUT) if not os.path.isfile(folder)]

    # Recorremos el numero de secuencias, leemos el fichero GT y vamos guardando en un diccionario las características
    for i in range(1, len(folders) + 1):
        files = [0 for file in os.listdir(constants.INPUT + str(folders[i - 1]) + '/data/') if
                 re.match(r'GT+.*\.mat', file + '/' + str(i))]
        try:
            for j in range(1, len(files) + 1):
                    mat = sio.loadmat(constants.INPUT + str(i) + '/data/GT' + str(j) + '.mat')
                    for im in images:
                        img = im.split('/')[-1]
                        ext = img.split('.')[0]
                        try:
                            if not ext in dicc:
                                dicc[ext] = []
                            dicc[ext].append(np.array(mat[str('I_' + ext)][0][0][0][0]))
                        except:
                            continue
        except:
            print "No such files of Ground Truth"
            logger.info("No such files of Ground Truth")
    return dicc


def __read_matlab_prototypes(opcion=1):
    """
    Esta función lee las bounding box que hemos guardado al obtener las características
    :return:
    """
    logger = logs.get_logger()
    dicc = {}
    # La opcion 1 tiene los ficheros de OpenFace y la 2 de FaceDetection
    if opcion == 1:
        path = constants.MAT
    elif opcion == 2:
        path = constants.MAT_MATLAB

    if exists(path):
        # Obtenemos los ficheros .mat y los guardamos en un diccionario
        for folder in os.listdir(path):
            if re.match(r'[0-9]+.*\.mat', folder):
                mat = sio.loadmat(path + folder)
                strI = folder[:-4]
                try:
                    if len(mat['data'] > 0):
                        dicc[strI] = mat['data']
                except:
                    logger.info("No se ha podido insertar: {}".format(strI))

    return dicc


def __list_of_files():
    """
    En esta función preparamos un objeto Detection con namedtuple que contendrá la ruta de la imagen y las bounding boxes
    para el GT y el de la predicción del algoritmo de reconocimiento facial
    :return:
    """
    # define the `Detection` object
    Detection = namedtuple("Detection", ["image_path", "gt", "pred"])

    prototypes = __read_matlab_prototypes(opcion=1)
    gt = __read_matlab()

    list = []
    if len(gt) > 0:
        for key in gt:
            sublistGT = []
            sublistPR = []
            for g in gt[key]:
                sublistGT.append([int(i) for i in np.array(g).tolist()])
            if key in prototypes:
                for rect in prototypes[key]:
                    sublistPR.append([int(i) for i in np.array(rect).tolist()])
                list.append(Detection(constants.IMAGES + key + '.jpg', sublistGT, sublistPR))

    return list


def __bb_intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = (xB - xA + 1) * (yB - yA + 1)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    if(iou > 0.0):
        return iou
    else:
        return 0.0


def detect():
    """
    En esta funcion calcularemos los TP, TN, FP, FN, precision y el recall.
    :return:
    """
    logger = logs.get_logger()
    logger.info('Calculate TP, TN, FP, FN...')
    list = __list_of_files()

    TP, TN, FP, FN = 0.0, 0.0, 0.0, 0.0

    if len(list) > 0:
        for detection in list:
            # load the image
            image = cv2.imread(detection.image_path)

            # draw the ground-truth bounding box along with the predicted
            # bounding box
            for l in detection.gt:
                cv2.rectangle(image, tuple(l[:2]), (l[0] + l[2], l[1] + l[3]), (0, 255, 0), 2)

            # For Ramanan
            # for l in detection.pred:
            #     cv2.rectangle(image, tuple(l[:2]), (l[0] + l[2], l[1] + l[3]), (0, 0, 255), 2)

            # For OpenFace structure: [left, top, right, bottom]
            for l in detection.pred:
                cv2.rectangle(image, tuple(l[:2]), tuple(l[2:]), (0, 0, 255), 2)

            # height, width = image.shape[:2]
            # res = cv2.resize(image, (width/2, height/2), interpolation=cv2.INTER_CUBIC)
            # cv2.imshow(detection.image_path, res)
            # cv2.waitKey(0)

            # Calculamos la interseccion
            for g in detection.gt:
                gt_int = [g[0], g[1], g[0] + g[2], g[1] + g[3]]
                for p in detection.pred:
                    iou = __bb_intersection_over_union(gt_int, p)
                    if iou > 0.1:
                        TP += 1

            # Si tenemos mas bounding boxes de GT que del algoritmo tendremos un FN
            if len(detection.gt) > len(detection.pred):
                FN += (len(detection.gt) - len(detection.pred))

            # Si tenemos menos bounding boxes de GT que del algoritmo tendremos un FP
            if len(detection.gt) < len(detection.pred):
                FP += (len(detection.pred) - len(detection.gt))

        folders = [constants.INPUT + folder for folder in os.listdir(constants.INPUT) if not os.path.isfile(folder)]
        subfolder = [folder + '/' + i for folder in folders for i in os.listdir(folder) if not os.path.isfile(folder)]


        if exists(constants.OUTPUT):
            # Obtenemos todas las imágenes (tengan o no tengan cara)
            GT = [folder + '/' + i for folder in subfolder for i in os.listdir(folder) if
                  os.path.isfile(folder + '/' + i) and re.match(r'[0-9]+.*\.jpg', i)]
            # Obtenemos las imagenes que tienen cara
            PR = [folder for folder in os.listdir(constants.OUTPUT) if re.match(r'[0-9]+.*\.jpg', folder)]
            # La resta nos dara los TN
            TN = (len(GT) - len(PR))

            logger.info('Total encontrado:')
            logger.info('----- True Positive: {0:0.0f}'.format(TP))
            logger.info('----- True Negative: {0:0.0f}'.format(TN))
            logger.info('----- False Positive: {0:0.0f}'.format(FP))
            logger.info('----- False Negative: {0:0.0f}'.format(FN))

            print ('Total encontrado:')
            print ('----- True Positive: {0:0.0f}'.format(TP))
            print ('----- True Negative: {0:0.0f}'.format(TN))
            print ('----- False Positive: {0:0.0f}'.format(FP))
            print ('----- False Negative: {0:0.0f}'.format(FN))

            # Calculamos la precision y el recall
            precision = TP / (TP + FP)
            recall = TP / (TP + FN)

            # f1 = (2 * TP)/((2 * TP) + FP + FN) # Dos formas de calcularlo
            f1 = 2 * ((precision * recall)/(precision + recall))

            logger.info('Precision: {0:0.3f}'.format(precision))
            logger.info('Recall: {0:0.3f}'.format(recall))
            logger.info('F-Measure: {0:0.3f}'.format(f1))

            print ('Precision: {0:0.3f}'.format(precision))
            print ('Recall: {0:0.3f}'.format(recall))
            print ('F-Measure: {0:0.3f}'.format(f1))
        else:
            print "No such folder output, try execute the first item of menu."
            logger.info("No such folder output, try execute the first item of menu.")
