# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import scipy.io as sio
from os import makedirs, listdir
import re
import cv2
from os.path import exists
import openface
import os
from src.util import constants
import numpy as np
from src.util import tsne
from src.model import agrupacion
from src.model import evaluacion
import time


fileDir = os.path.dirname(os.path.realpath(__file__))
net = openface.TorchNeuralNet(os.path.join(fileDir, '../../models/openface/nn4.small2.v1.t7'), 96)
dlib = openface.AlignDlib(os.path.join(fileDir, '..', '..', 'models', 'dlib', 'shape_predictor_68_face_landmarks.dat'))



def find_face(file):
    """
    Este método busca si en la imagen entrante existe alguna cara
    :param file:
    :return:
    """
    face = cv2.imread(file)
    rbImg = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
    bb = dlib.getLargestFaceBoundingBox(rbImg)
    if bb is None:
        print "Unable to find a face in " + file
        os.remove(file)
    return bb

def featur(file):
    """
    Este método retorna las características para la imagen entrada por parámetros
    :param file:
    :return:
    """
    bb = find_face(file)
    if bb is not None:
        face = cv2.imread(file)
        rbImg = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
        alignedFace = dlib.align(96, rbImg, bb,
                                  landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
        if alignedFace is None:
            raise Exception("Unable to align image: {}".format(file))

        rep = net.forward(alignedFace)
        return rep
    return None


def function():
    """
    Esta función lee los prototipos obtenidos con BoT y recorta la imagen para obtener la cara
    más tarde guardamos esta cara en la secuencia correspondinte. Por último, le aplicamos OpenFace
    para descartar posibles errores y asegurarnos que tenemos una cara en la imagen.
    :return:
    """
    start_time = time.time()
    means = []
    for dir in listdir(constants.PROTOTYPES):
        for pr in listdir(constants.PROTOTYPES + dir + '/'):
            if re.match(r'prFinal+[0-9].*\.mat', pr):
                mat = sio.loadmat(constants.PROTOTYPES + dir + '/' + pr)

                feat = []
                for im in listdir(constants.INPUT + dir + '/data/'):
                    if re.match(r'[0-9]+.*\.jpg', im):
                        name = 'I_' + im[:-4]
                        if name in mat:
                            res = cv2.imread(constants.INPUT + dir + '/data/' + im)
                            x =  mat[name][0][0][0][0][0]
                            y =  mat[name][0][0][1][0][0]
                            w =  mat[name][0][0][2][0][0]
                            h = mat[name][0][0][3][0][0]

                            top = int(y) - int(300)
                            bottom = (int(y) + int(h)) + int(300)
                            left = int(x) - int(300)
                            right = (int(x) + int(w)) + int(300)

                            if top < 0: top = 0
                            if left < 0: left = 0

                            height, width, channels = res.shape
                            if bottom > height: bottom = height
                            if right > width: right = width

                            crop_img = res[top:bottom, left:right]
                            if not exists(constants.BOT + dir + '/' + pr[7:-4]):
                                makedirs(constants.BOT + dir + '/' + pr[7:-4])
                            folder = constants.BOT + dir + '/' + pr[7:-4] + '/' + im

                            cv2.imwrite(folder, crop_img)  # Guardamos solamente la cara
                            cv2.imwrite(constants.BOUNDINGBOX + im, crop_img)

                            find_face(constants.BOUNDINGBOX + im)
                            rep = featur(folder)
                            if rep is not None:
                                feat.append(rep)
                if len(feat) > 0:
                    count = 0
                    for i in range(0, len(feat)):
                        count += feat[i]
                    mean = count/len(feat)
                    means.append(mean)

    features = [[0 for m in range(len(means))] for x in range(len(means))]
    for i in range(len(means)):
        for j in range(len(means)):
            d = means[i] - means[j]
            features[i][j] = np.dot(d, d)

    X = tsne.tsne(np.asarray(features))

    agrupacion.cluster_by_file(X)

    elapsed_time = time.time() - start_time
    evaluacion.eval()
    print elapsed_time
function()




