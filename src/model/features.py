# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import argparse
import logging as log
import math
import numpy as np
import os
import re
import scipy.io as sio
import shutil
import time
from os import makedirs, listdir
from os.path import isfile, exists
from threading import Thread

import cv2
import openface

from src.util import constants
from src.util import logs


def __mkdir(folder):
    """
    Función de soporte para eliminar carpeta en caso que exista y volverla a crear.
    :param folder:
    :return:
    """
    if exists(folder):
        shutil.rmtree(folder)
    makedirs(folder)


def __config():
    """
    Esta función leera los parámetros introducidos necesarios para ejecutar OpenFace.
    También controla que exista la carpeta contenedora de imágenes para anlizar
    y en caso que no exista la carpeta de salida, la crea.
    :return:
    """
    logger = logs.get_logger()
    fileDir = os.path.dirname(os.path.realpath(__file__))
    dlibModelDir = os.path.join(fileDir, '..', '..', 'models', 'dlib')
    openfaceModelDir = os.path.join(fileDir, '..', '..', 'openface')

    parser = argparse.ArgumentParser(description='Proceso analista de imagenes')
    parser.add_argument('--dlibFacePredictor', type=str, help="Path to dlib's face predictor.",
                        default=os.path.join(dlibModelDir, "shape_predictor_68_face_landmarks.dat"))
    parser.add_argument('--networkModel', type=str, help="Path to Torch network model.",
                        default=os.path.join(openfaceModelDir, 'nn4.small2.v1.t7'))
    parser.add_argument('--imgDim', type=int,
                        help="Default image dimension.", default=96)

    arguments = parser.parse_args()

    if not exists(constants.INPUT):
        logger.error("No existe la ruta contenedora de imagenes")
        raise Exception("No existe la ruta contenedora de imagenes")

    # Controlar que la carpeta existe.
    if not exists(constants.OUTPUT):
        logger.info("No existe la carpeta de salida, la creamos")
        makedirs(constants.OUTPUT)
    os.system('sudo chmod -R 777 ' + constants.OUTPUT)
    return arguments


def __get_features(img_path, dlib, torch, imgDim):
    """
    Está función será llamada para cada imágen, buscará todas las caras de la imagen
    y extraerá la matriz de características guardandola en un fichero. Solamente nos
    interesan las caras que estan interactuando, por tanto, descartaremos las caras
    que esten lejos. Y agrandaremos la bounding box y lo guardamos en una carpeta
    para poder visualizarlo al finalizar. También guardamos los datos relacionados
    con la bounding box para poder analizar el algoritmo más tarde.
    :param img_path:
    :param dlib:
    :param torch:
    :param imgDim:
    :return:
    """
    logger = logs.get_logger()
    logger.debug("Processing {}.".format(img_path))
    # Leemos la imagen
    res = cv2.imread(img_path)
    # Comprobamos que la podemos leer correctamente.
    if res is None:
        logger.error("Unable to load image: {}".format(img_path))
    else:
        # Guardamos la imagen leida con el fin de poder tener todas las imágenes que han sido leidas
        cv2.imwrite(constants.IMAGES + img_path.split('/')[-1], res)
        rgbImg = cv2.cvtColor(res, cv2.COLOR_BGR2RGB)
        # rgbImg = cv2.cvtColor(res, cv2.COLOR_GRAY2RGB)
        logger.debug("  + Original size: {}".format(rgbImg.shape))

        start = time.time()
        # Buscamos TODAS las caras de la imagen.
        bb = dlib.getAllFaceBoundingBoxes(rgbImg)
        if bb is None:
            # En caso de no tener nos mostrará el siguiente mensaje por logs
            logger.info("Unable to find a face: {}".format(img_path))
        else:
            name = img_path.split('/')[-1]
            nameFolder = img_path.split('/')[-3]

            # Creamos un diccionario para guardar las características de la bounding box que
            # mas tarde guardaremos en un fichero .mat
            prototype = dict()
            prototype['data'] = []

            # Recorremos para cada rectángulo encontrado en la imagen
            for k, d in enumerate(bb):
                # Calculamos la diagonal del rectángulo para impedir procesar imágnes pequeñas
                # ya que SOLO queremos las que interactúan
                dist = math.sqrt(pow((d.right() - d.left()), 2) + pow((d.bottom() - d.top()), 2))
                # Solo trataremos las imágenes superiores a 150.0
                if dist > 150.0:
                    # Agrandamos el tamaño de la bounding box
                    top = (int(d.top()) - 300)
                    bottom = (int(d.bottom()) + 300)
                    right = (int(d.right()) + 300)
                    left = (int(d.left()) - 300)

                    # Comprobamos que al agrandar el tamaño de la bounding box no nos pasamos el tamaño de la imagen
                    if top < 0: top = 0
                    if left < 0: left = 0

                    height, width, channels = rgbImg.shape
                    if bottom > height: bottom = d.bottom()
                    if right > width: right = d.right()

                    logger.info("Detection {}: Left: {} Top: {} Right: {} Bottom: {} Name: {}".format(k, left, top, right,
                                                                                                    bottom, name))
                    # Vamos añadiendo las características de la bounding box en el .mat
                    prototype['data'].append([left, top, right, bottom])

                    # Recortamos la imagen con el tamaño de la bounding box y lo guardamos en una carpeta
                    crop_img = res[top:bottom, left:right]
                    folder = constants.BOUNDINGBOX + str(k) + '_' + name
                    cv2.imwrite(folder, crop_img)  # Guardamos solamente la cara

                    # Guardamos la imagen en la carpeta de salida, así podremos ver de forma sencilla las imágenes que contienen cara/s.
                    if not exists(constants.OUTPUT + name):
                        cv2.imwrite(constants.OUTPUT + name, res)

                    # Pintamos el rectángulo encontrado
                    cv2.rectangle(res, (left, top), (right, bottom), (255, 0, 255), 2)
                    cv2.imwrite(constants.DETECTION + name, res)  # Guardamos la imagen con el rectangulo

                    logger.debug("  + Face detection took {} seconds.".format(time.time() - start))

                    start = time.time()
                    # Ahora buscamos las características de cada cara
                    alignedFace = dlib.align(imgDim, rgbImg, d, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
                    if alignedFace is None:
                        logger.error("Unable to align image: {}".format(img_path))
                    else:
                        logger.debug("  + Face alignment took {} seconds.".format(time.time() - start))

                        start = time.time()
                        rep = torch.forward(alignedFace)

                        logger.debug("  + OpenFace forward pass took {} seconds.".format(time.time() - start))
                        logger.debug("Representation:")
                        logger.debug(rep)
                        logger.debug("-----\n")
                        # Una vez tenemos las características, las guardamos en un fichero .txt
                        # Guardamos las características todas juntas y también en su secuencia dentro de la carpeta data.
                        # if not exists(constants.FOLDERFEATURES + '/data/' + nameFolder + '/'):
                        #     makedirs(constants.FOLDERFEATURES + '/data/' + nameFolder + '/')
                        # filename = constants.FOLDERFEATURES + '/data/' + nameFolder + '/' + str(k) + '_' + name[:-4] + '.txt'
                        # if not exists(filename):
                        #     target = open(filename, 'a')
                        # target = open(filename, 'rw+')
                        # np.asarray(rep).tofile(target, ',')
                        # target.close()

                        filename = constants.FOLDERFEATURES + str(k) + '_' + name[:-4] + '.txt'
                        if not exists(filename):
                            target = open(filename, 'a')
                        target = open(filename, 'rw+')
                        np.asarray(rep).tofile(target, ',')
                        target.close()

                        logger.info('Creat fitxer de caracteristiques per la imatge {}'.format(name))
            # # Guardamos las características de la bounding box en un fichero .mat
            # if not exists(constants.MAT + '/data/' + nameFolder + '/'):
            #     makedirs(constants.MAT + '/data/' + nameFolder + '/')
            # sio.savemat(constants.MAT + '/data/' + nameFolder + '/' + name[:-4] + '.mat', prototype)
            #
            # sio.savemat(constants.MAT + name[:-4] + '.mat', prototype)


def get_faces():
    """
    Esta función va recorriendo las imágenes y llama a la función para obtener las características
    Utilizamos threads para agilizar y hacerlo más rápido
    :return:
    """
    logger = logs.get_logger()
    arguments = __config()
    dlib = openface.AlignDlib(os.path.join('../../models/dlib/', arguments.dlibFacePredictor))
    torch = openface.TorchNeuralNet(arguments.networkModel, arguments.imgDim)

    logger.info('Encontrando caras...')

    # Llamamos a la función de apoyo para limpiar las carpetas que utilizaremos.
    __mkdir(constants.DETECTION)
    __mkdir(constants.BOUNDINGBOX)
    __mkdir(constants.OUTPUT)
    __mkdir(constants.FOLDERFEATURES)
    __mkdir(constants.MAT)
    __mkdir(constants.IMAGES)
    __mkdir(constants.REP)

    # Leemos las imagenes de las secuencias
    folders = [constants.INPUT + folder for folder in listdir(constants.INPUT) if not isfile(folder)]
    subfolder = [folder + '/' + i for folder in folders for i in listdir(folder) if not isfile(folder)]
    # images = [folder + '/' + i for folder in folders for i in listdir(folder) if isfile(folder + '/' + i)]
    images = [folder + '/' + i for folder in subfolder for i in listdir(folder) if
              isfile(folder + '/' + i) and re.match(r'[0-9]+.*\.jpg', i)]

    # Recorremos las imagenes creando threa
    for f in images:
        t = Thread(target=__get_features, args=(f, dlib, torch, arguments.imgDim,))
        t.start()
        logger.debug('Hilo %s con imagen %s', t.getName(), f)
        t.join()
        # 1 hilo:
        #__get_features(f, dlib, torch, arguments.imgDim)