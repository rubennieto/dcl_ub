# -*- coding: utf-8 -*-

#################################################
##
## Codigo realizado por Rubén Nieto Vargas
## Estudiante de la Universidad de Barcelona
## Trabajo Final de Grado
## 26 de enero de 2017
##
#################################################

import test
from os.path import exists

from src.model import classifier
from src.model import agrupacion
from src.model import distance
from src.model import features
from src.model import multidimensional_scaling as mds
from src.model import evaluacion
from src.model import test
from src.util import constants, mail
from src.util import logs
from src.view import visualizacion as vs


def print_menu():
    """
    Menu principal de la aplicación
        1. Calcular características
        2. Calcular y crear la matriz de distancia y crear un excel con los datos
        3. Calcular, con el método que elijamos, una reducción de dimensionalidad
        4. Calcular, con el método que elijamos, los diferentes clusters
        5. Calcular TP, TN, FP, FN, accuracy y recall
        6. Visualizar a las personas encontradas
    :return:
    """
    print 30 * "-", "MENU", 30 * "-"
    print "1. Features"
    print "2. Distances"
    print "3. Multi - dimensional scaling"
    print "4. Clusters"
    print "5. Calculate accuracy and recall"
    print "6. Show to the differents persons"
    print "7. Evaluation of clustering"
    print "8. Exit"
    print 66 * "-"


def print_menu_reduction():
    """
    Submenu que muestra las opciones para la reducción de dimensionalidad
    :return:
    """
    print 19 * "-", " MULTI-DIMENSIONAL SCALING ", 20 * "-"
    print "1. PCA"
    print "2. t-SNE"
    print "3. Go back"
    print 66 * "-"


def print_submenu():
    """
    Submenú que muestra las diferentes opciones que tenemos para hacer clustering
    :return:
    """
    print 28 * "-", " CLUSTERS ", 28 * "-"
    print "1. Clustering - no supervisado"
    print "2. Clasificador - supervisado"
    print "3. Go back"
    print 66 * "-"


def print_submenu_evaluation():
    """
        Submenú que muestra las diferentes opciones que tenemos para evaluar el cluster
        :return:
        """
    print 30 * "-", "EVALUATION of CLUESTERING", 30 * "-"
    print "1. Evaluate clustering"
    print "2. Evaluate classifier"
    print "3. Go back"
    print 66 * "-"


def reduction():
    """
    Función para elegir que método de reducción de dimensionalidad queremos utilizar
    :return:
    """
    exit = False
    while not exit:
        print_menu_reduction()
        try:
            choice = raw_input("Enter your choice [1-3]: \n")
            choice = int(choice)
        except:
            print 'Option not available'

        if 1 <= choice <= 3:
            if choice == 1:
                print "PCA"
                mds.redimension(option=2)
            elif choice == 2:
                print "t-SNE"
                mds.redimension(option=1)
            elif choice == 3:
                print "Tornant al menu principal..."
                exit = True
        else:
            print 'Option not available'


def clusters():
    """
     Función para elegir qué método de clustering queremos utilizar
    :return:
    """
    logger = logs.get_logger()
    exit = False
    # try:
    while not exit:
        print_submenu()
        try:
            choice = raw_input("Enter your choice [1-3]: \n")
            choice = int(choice)
        except:
            print 'Option not available'

        if 1 <= choice <= 3:
            if choice == 1:
                print "Clustering - no supervisado"
                agrupacion.cluster_by_file(agrupacion.read_features())
            elif choice == 2:
                classifier.clas()
            elif choice == 3:
                print "Tornant al menu principal..."
                exit = True
        else:
            print 'Option not available'
    # except:
    #     print 'No such file of features, try execute the first item of menu'
    #     logger.error('No such file of features, try execute the first item of menu')


def evaluation():
    """
         Función para elegir qué método de clustering queremos utilizar
        :return:
        """
    exit = False
    while not exit:
        print_submenu_evaluation()
        try:
            choice = raw_input("Enter your choice [1-3]: \n")
            choice = int(choice)
        except:
            print 'Option not available'

        if 1 <= choice <= 3:
            if choice == 1:
                print "Evaluation of clustering"
                evaluacion.eval(True)
            elif choice == 2:
                print "Evaluation of classifier"
                evaluacion.eval(False)
            elif choice == 3:
                print "Tornant al menu principal..."
                exit = True
        else:
            print 'Option not available'


def menu():
    """
    Función para elegir que opción del menú ejecutar
    :return:
    """
    logger = logs.get_logger()
    exit = False
    while not exit:
        print_menu()
        try:
            choice = raw_input("Enter your choice [1-8]: \n")
            choice = int(choice)

        except:
            print 'Option not available'

        if 1 <= choice <= 8:
            if choice == 1:
                print "Calculate features of each faces"
                logger.info("Calculate features of each faces")
                features.get_faces()
            elif choice == 2:
                print "Calculate distance between each face"
                logger.info("Calculate distance between each face")
                distance.distance()
            elif choice == 3:
                print "Multi - dimensional Scaling..."
                logger.info('Multi - dimensional Scaling')
                reduction()
            elif choice == 4:
                print "Clustering"
                logger.info("Clustering")
                clusters()
            elif choice == 5:
                print "Caculate accuracy and precision"
                logger.info("Caculate accuracy and precision")
                test.detect()
            elif choice == 6:
                print "Show to the differents persons"
                logger.info("Show to the differents persons")
                vs.GUI()
            elif choice == 7:
                print "Evaluation of clustering"
                logger.info("Evaluation of clustering")
                evaluation()
            elif choice == 8:
                print "Good bye!"
                exit = True
        else:
            print 'Option not available'


def main():
    """
    Función principal de la aplicación, inicializamos los logs y llamamos al menú principal. Una vez acabamos envía un
    mail, o en caso de error, también envía un mail.
    :return:
    """
    try:
        LOG_FILENAME = constants.LOGS + constants.FILELOG
        logs.logs()
        logger = logs.get_logger()
        logger.info('------------ Inicio proceso ------------')
        menu()
        logger.info('------------ Fin proceso ------------')

        if exists(constants.CLUSTERS + constants.IMAGECLUSTERING):
            mail.send(files=[LOG_FILENAME, constants.CLUSTERS + constants.IMAGECLUSTERING])
        else:
            mail.send(files=[LOG_FILENAME])
    except:
        logger.exception("Ha ocurrido un error durante la ejecución, mirar logs para más información. ")
        mail.send(False, files=[LOG_FILENAME])
        raise Exception("Ha ocurrido un error durante la ejecución, mirar logs para más información")

if __name__ == "__main__":
    main()
